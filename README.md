# README – UE4 Third Person Item System Template - v0.2 #

This UE4 third person template presents an extendable item system that features base actors the player can interact with, pick up from the game world, drop, use.

The base on which this template is built upon is a light version of the UE4 Third Person template (limited to mouse and keyboard input).


## Template Features ##

* Developed and tested on Unreal Engine 4.15
* Both C++ and Blueprint based 
* Full in-code documentation
* Data driven
* Interaction with items in the game world (Interactable class)
* Store, use and drop items in the player’s inventory (Storable class)
* Inventory as ActorComponent with inheritance support (InventoryComponent class)
* Item Stacking
* Easy to set up, customize and extend
* Basic UMG user interface included to show some of the features


## How do I get set up? ##

### As Template ###
1. Download and unzip (or clone) this repository in your Unreal Engine 4 template folder (e.g. “C:\Program Files\Epic Games\UE_4.15\Templates”)
2. Create a new C++ Project using the Epic Games Launcher and selecting “TP Item System” as template
3. Open and Save “BP_NevLDGameInstance” and “BP_NevLDGameMode” (you should see them in the Content Browser - Blueprints folder) 
4. In *Edit/Project Settings/Maps & Modes* select “BP_NevLDGameMode” as Default GameMode and “BP_NevLDGameInstance” as Game Instance Class
5. You are ready to go!
### As Project ###
1. Download and unzip (or clone) this repository in your Unreal Engine 4 project folder
2. Delete the Content/TemplateDefs.ini file
3. Open “NevLD.uproject”
4. Answer Yes when the Missing NevLD Modules window appears
5. You are ready to go!
### Key mappings ###
* Keyboard I: Open/Close the Player's Inventory
* Keyboard F: Interact/Store items
* Double Left Click on stored items: Use item, if possible
* Right Click on stored items: Open item context menu


## Coming next... ##
...in no particular order, subject to change, and depending on my free time.

* Most common item’s usages implementation
* Gathering System
* Crafting System


## About the Author, the Project and You ##

Hi! I'm Claudio 'Nevril' Parisi.
I'm a computer scientist and I work as a software architect, not in the gaming industry (yet?).
I usually spend 50+hrs a week developing some financial application backend, mainly using Java, PL/SQL, whatever…
In those rare occasions where I have some time to spare, I like to experiment with game programming and design, a life long passion.

To me, this project is essentially a way to fix a goal and learn as much as possible while trying to reach it.
And what’s the best way to learn if not sharing and discuss your ideas with others?

And here you come!
Anytime you feel like you can contribute with something, feel absolutely free to do so.
Moreover, if you have any doubt or question, just ask! I'll try to answer as soon as possible to the best of my abilities.
Finally, any spellcheck is also appreciated. Not a native English speaker here.


## Contacts ##
* [Nevril on the Unreal Engine Forum](https://forums.unrealengine.com/member.php?535246-Nevril)
* [LinkedIn Profile](https://www.linkedin.com/in/claparisi?locale=en_US)


## ChangeLog ##

### Version 0.2 ###
* Item Stacking support
* The inventory is now an ActorComponent which can be inherited and modified both in C++ & Blueprints
* Removed inventory parameters from the controller: they're now available on the InventoryComponent
* Included additional parameters in the Storable datatable to support stacking and better define an item (e.g. Category, Rarity)
* Minor changes to the included UMG interface to show some of the v0.2 features

### Version 0.1 ###
* Initial release
* Basic item pick up, drop, use support
* Basic UMG inventory interface
// Copyright © 2016-2017, Claudio Parisi. All rights reserved.

#pragma once

#include "ItemSystem/Interactable.h"
#include "Engine/DataTable.h"
#include "Storable.generated.h"

/** Enum of all the Storable categories available in game.
*
* Can be used for inventories that organize items according to a category.
* To order in which they are declared will be reflected when sorting the base Inventory by Category.
*/
UENUM(BlueprintType)
enum class EStorableCategory : uint8
{
	DummyCategory		UMETA(DisplayName = "DummyCategory"),
	Consumable			UMETA(DisplayName = "Consumable"),
	Equipment			UMETA(DisplayName = "Equipment"),
	CraftingMaterial	UMETA(DisplayName = "CraftingMaterial"),
	QuestItem			UMETA(DisplayName = "QuestItem"),
	LoreBook			UMETA(DisplayName = "LoreBook")
};

/** Enum of all the Storable rarities available in game.*/
UENUM(BlueprintType)
enum class EStorableRarity : uint8
{
	Generic				UMETA(DisplayName = "Generic"),
	Common				UMETA(DisplayName = "Common"),
	Rare				UMETA(DisplayName = "Rare"),
	Epic				UMETA(DisplayName = "Epic")
};

/** Structure describing a row in the Item DataTable. */
USTRUCT(BlueprintType)
struct FStorableRow : public FTableRowBase {
	GENERATED_BODY()

public:

	FStorableRow() {
		Name = FText::FromString("Dummy Item");
		Storable = NULL;
		MaxStackSize = 1;
		StorableCategory = EStorableCategory::DummyCategory;
		bUsable = false;
		bDestroyOnUse = true;
		Description = FText::FromString("Dummy Item Description");
		Value = 0;
	}

	/** The unique identifier of this item. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		FName ItemID;

	/** The in-game name of this item. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		FText Name;

	/** The thumbnail that'll be displayed in game (e.g. in the inventory) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		UTexture2D* Thumbnail;

	/** Item implementation reference. (Subclass of Storable) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		TSubclassOf<class AStorable> Storable;

	/** The maximum amount of items that can be stacked together
	* 0 or 1 if the Item cannot be stacked.
	*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		uint8 MaxStackSize;

	/** The Category of this item */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		EStorableCategory StorableCategory;

	/** Whether this item can be used by the player */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		bool bUsable;

	/** Whether this item is destroyed upon usage (Ignored if bUsable == false) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		bool bDestroyOnUse;

	/** The Rarity of this item */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		EStorableRarity StorableRarity;

	/** The value of this item for the game economy system */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		int32 Value;

	/** The in game description of this item. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		FText Description;

	/** Defines the equality among two items. */
	bool operator==(const FStorableRow Item) const {
		return ItemID == Item.ItemID;
	}

	/** Defines the absolute order among two items. */
	bool operator<(const FStorableRow Item) const {
		return ItemID.Compare(Item.ItemID) < 0;
	}

};

/**
* This class represents an Interactable a player can store in its Inventory.
* The Storable can also be usable.
*
* Examples are crafting ingredients, potions, etc.
*/
UCLASS()
class NEVLD_API AStorable : public AInteractable
{
	GENERATED_BODY()
	
public:

	AStorable();

protected:

	/** The FItemRow ItemID corresponding to this Storable */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FName ItemID;

	/** The actual amount of items represented by this Storable at a given time (i.e. when spawned in the game world) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	uint8 StackSize;
	
	/** Called when we use this Storable.
	*
	* In its base implementation, once this Storable is used it always gets destroyed.
	* If a Controller is provided and bRemoveFromInventory == true, it is also removed from the player's inventory, if found.
	* A Designer can choose to implement it using both blueprints or C++ (overriding the _Implementation)
	*
	* @param Controller The Controller that has this Storable in its Inventory, if any
	* @param bRemoveFromInventory Whether this Storable will be removed from the Controller's Inventory once used.
	*/
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Item Functions")
	void Use(ANevLDPlayerController* Controller, bool bRemoveFromInventory);
	virtual void Use_Implementation(ANevLDPlayerController* Controller, bool bRemoveFromInventory);

	virtual void Interact_Implementation(ANevLDPlayerController* Controller) override;

public:

	
	/** Gets the amount of items represented by this Storable at a given time (i.e. when spawned in the game world).
	*
	* Can be used, for example, on pickup to understand "how many" of this item will be stored in the players inventory.
	*
	* @return the current stack size of this Storable
	*/
	UFUNCTION(BlueprintCallable, Category = "Item Functions")
	uint8 GetCurrentStackSize() {
		return StackSize;
	}
	
	/** Sets the amount of items represented by this Storable at a given time (i.e. when spawned in the game world).
	*
	* Can be used, for example, on drop to understand "how many" of this item will be dropped in a single Storable instance.
	*/
	UFUNCTION(BlueprintCallable, Category = "Item Functions")
	void SetCurrentStackSize(uint8 Size) {
		StackSize = Size;
	}
};

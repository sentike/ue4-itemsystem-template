// Copyright © 2016-2017, Claudio Parisi. All rights reserved.

#pragma once

#include "Engine/GameInstance.h"
#include "Engine/DataTable.h"
#include "ItemSystem/Storable.h"
#include "NevLDGameInstance.generated.h"

/**
 * A GameInstance can be useful when we need to manage resources throughout the game.
 *
 * It basically acts as a Singleton (and technically seems to be).
 * For example we are using it as the only access point to load and store data to our DataTables.
 * Eventually, can be used to instantiate and delete other Singletons if they're needed (not to be abused).
 */
UCLASS()
class NEVLD_API UNevLDGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:

	/** Retrieves a Storable using its ID */
	struct FStorableRow* GetStorableFromDataTable(FName ItemID);
	
protected:

	/** The DataTable containing all the Storables of the game */
	UPROPERTY(EditDefaultsOnly)
	UDataTable* StorableDataTable;
};
